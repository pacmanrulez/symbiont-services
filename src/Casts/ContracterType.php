<?php

namespace Symbiont\Services\Casts;

enum ContracterType: int {

    case Static = 0;
    case NoneStatic = 1;

}