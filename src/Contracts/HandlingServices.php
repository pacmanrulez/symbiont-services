<?php

namespace Symbiont\Services\Contracts;

use Illuminate\Database\Eloquent\Model;

interface HandlingServices {

    public static function services(string $model, array $services);
    public static function service(string $service, string $model): mixed;
    public static function locateService(string $called, string $abstract, ?Model $instance = null, array $input = []): Serviceable;

}