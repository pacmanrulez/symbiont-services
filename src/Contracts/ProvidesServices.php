<?php

namespace Symbiont\Services\Contracts;

// contract to ProvidesServices
use Illuminate\Database\Eloquent\Model;

interface ProvidesServices {

    public function serve(string $abstract): mixed;
    public static function serving(string $abstract): mixed;

}