<?php

namespace Symbiont\Services\Contracts;

use Symbiont\Services\Casts\ContracterType;

interface Serviceable {

    public static function make(): mixed;
    public function isType(ContracterType $type): bool;

}