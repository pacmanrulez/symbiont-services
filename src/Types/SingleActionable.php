<?php

namespace Symbiont\Services\Types;

interface SingleActionable extends Actionable { }