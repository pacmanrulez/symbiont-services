<?php

namespace Symbiont\Services;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Pipeline\Pipeline;

use Symbiont\Services\Contracts\Serviceable;
use Symbiont\Services\Exceptions\ServiceNotFound;
use Symbiont\Services\Pipes\Location\{
    LocatorAbstractIsClass, LocatorModel, LocatorServiceContainer, LocatorServiceHandler, Traveler
};
use Symbiont\Services\Pipes\Transportable;

final class ServiceHandler implements Contracts\HandlingServices {

    use Concerns\GoesThrough;

    protected static $services = [];

    /**
     * @param array $services
     * @return void
     */
    public static function services(string $model, array $services) {
        if(! array_key_exists($model, self::$services)) {
            self::$services[$model] = [];
        }
        self::$services[$model] = array_merge(self::$services[$model], $services);
    }

    /**
     * @param string $service
     * @param string|null $model
     * @return mixed
     * @todo: refactor
     */
    public static function service(string $service, string|null $model): mixed {

        if($model &&
            array_key_exists($model, self::$services) &&
            array_key_exists($service, self::$services[$model])) {
                return self::$services[$model][$service];
        }

        if(! $model &&
            array_key_exists('*', self::$services) &&
            array_key_exists($service, self::$services['*'])) {
                return self::$services['*'][$service];
        }

        return null;
    }

    /**
     * Handle service location.
     * @param string $abstract
     * @param Model|null $instance
     * @return mixed
     * @throws \Exception
     */
    public static function locateService(string $called, string $abstract, ?Model $instance = null, array $input = []): Serviceable {
        return app(Pipeline::class)
            ->send(app(Transportable::class, [
                'called' => $called,
                'abstract' => $abstract,
                'parameters' => [
                    'model' => $called,
                    'instance' => $instance,
                    'input' => $input
                ]
            ]))
            ->through([
                LocatorModel::class, // @todo: interface_exists to distinguish string names from contracts
                LocatorServiceHandler::class,
                LocatorServiceContainer::class,
                // last resort
                LocatorAbstractIsClass::class
            ])
            ->then(function($sequenced) {
                if($sequenced instanceof Transportable) {
                    throw new ServiceNotFound($sequenced->abstract);
                }

                return $sequenced;
            });
    }
}