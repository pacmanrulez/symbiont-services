<?php

namespace Symbiont\Services;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\ServiceProvider;
use Symbiont\Services\Contracts\HandlingServices;
use Symbiont\Services\Pipes\Location\Traveler;
use Symbiont\Services\Pipes\Transportable;

class ServicesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/../config/config.php' => config_path('service-contracter.php'),
            ], 'config');
        }

    }

    /**
     * Register the application services.
     */
    public function register()
    {
        // Automatically apply the package configuration
        $this->mergeConfigFrom(__DIR__.'/../config/config.php', 'service-contracter');

        $this->app->bind(HandlingServices::class, ServiceHandler::class);
        $this->app->bind(Transportable::class, Traveler::class);

        // Register the main class to use with the facade
        $this->app->singleton('service-contracter', function () {
            return new Services;
        });
    }
}
