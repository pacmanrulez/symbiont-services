<?php

namespace Symbiont\Services;

use Illuminate\Database\Eloquent\Model;
use Symbiont\Services\Casts\ContracterType;

abstract class Contracter
    implements Contracts\Serviceable {

    use Concerns\GoesThrough;

    public readonly ContracterType $_type;

    public readonly string $model;
    protected ?Model $instance;
    protected array $input;

    /**
     * Limit debug_backtrace where we expect the model to be second in line,
     * everything else needs to fail
     */
    const PROP_TRACE_LIMIT = 2;

    public function __construct(string $model, ?Model $instance = null, array $input = []) {
        $this->_type = $instance ?
            ContracterType::NoneStatic : ContracterType::Static;
        $this->model = $model;
        $this->instance = $instance;
        $this->input = $input;
    }

    /**
     * @return Model|null
     */
    final public function getInstance(): ?Model {
        return $this->instance;
    }

    /**
     * @param ContracterType $type
     * @return bool
     */
    final public function isType(ContracterType $type): bool {
        return $this->_type === $type;
    }

    public function pingPong() {
        return [
            'service' => $this::class,
            'model' => $this->model,
            'instance' => $this->instance,
            // debug
            '_type' => $this->_type,
            '_debug' => debug_backtrace(0|1, 3)
        ];
    }

    /**
     * Creates the requested service tracing back the original caller (model)
     * @return mixed
     * @throws \Exception
     */
    public static function make(): mixed {
        $traced = static::traceCaller(
            // catching and returning the last entry of debug_backtrace on the fly, suppressing
            // any warnings on its path
            ($trace = @debug_backtrace(
                DEBUG_BACKTRACE_IGNORE_ARGS|DEBUG_BACKTRACE_PROVIDE_OBJECT,
                static::PROP_TRACE_LIMIT))[count($trace)-1]);

        return app(static::class, $traced);
    }

    /**
     * @param array|null $trace
     * @return array
     * @throws \Exception
     */
    protected static function traceCaller(?array $trace): array {
        return !$trace ?
            throw new Exceptions\TraceLost() : [
                'model' => $trace['class'],
                'instance' => array_key_exists('object', $trace) ?
                    $trace['object'] :
                        null
            ];
    }

    /**
     * General purpose exception as a filter when debugging
     * @param string $name
     * @param array $arguments
     * @return void
     */
    public function __call(string $name, array $arguments) {
        if(substr($name, 0, 1) === '_') {
            $name = str($name)->replace('_', '')->toString();
        }

        if(! method_exists($this, $name)) {
            throw new Exceptions\UnknownServiceableMethod($this, $name);
        }

        return app()->call([$this, $name], $arguments);
    }

}