<?php

namespace Symbiont\Services\Exceptions;

use Exception;

class ServiceNotFound extends Exception {

    public function __construct(string $service) {
        parent::__construct('Unable to resolve service ' . $service);
    }

}