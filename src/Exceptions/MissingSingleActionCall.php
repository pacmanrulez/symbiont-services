<?php

namespace Symbiont\Services\Exceptions;

use Exception;
use Symbiont\Services\Pipes\Validation\ValidateSingleActionables;

class MissingSingleActionCall extends Exception {

    public function __construct(string $service) {
        parent::__construct('Service ' . $service . ' missing implementation of `__invoke()` or `'.ValidateSingleActionables::METHOD_HANDLE.'()`');
    }

}