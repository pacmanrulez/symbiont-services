<?php

namespace Symbiont\Services\Exceptions;

use Exception;

class ServiceRequiresInstance extends Exception {

    public function __construct($service) {
        parent::__construct('Service ' . $service .' requires a Model instance and cannot be called statically');
    }

}