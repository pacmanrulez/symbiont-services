<?php

namespace Symbiont\Services\Exceptions;

class UnknownServiceableMethod extends \Exception {

    public function __construct($service, $method)
    {
        parent::__construct("Servicable ".$service::class." for ".$service->model." does not have a method called: `{$method}`");
    }

}