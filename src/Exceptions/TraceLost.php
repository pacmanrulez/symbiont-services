<?php

namespace Symbiont\Services\Exceptions;

use Exception;

class TraceLost extends Exception {

    public function __construct() {
        parent::__construct('Trace lost');
    }

}