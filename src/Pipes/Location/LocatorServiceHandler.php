<?php

namespace Symbiont\Services\Pipes\Location;

use Symbiont\Services\Pipes\Transportable;
use Symbiont\Services\ServiceHandler;

class LocatorServiceHandler {

    public function handle(Transportable $transporter, \Closure $next) {
        if($service = ServiceHandler::service($transporter->abstract, $transporter->called)) {
            return app($service, $transporter->parameters);
        }

        return $next($transporter);
    }

}