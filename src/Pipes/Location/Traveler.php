<?php

namespace Symbiont\Services\Pipes\Location;

use Symbiont\Services\Pipes\Transportable;

readonly class Traveler implements Transportable {

    public string $model;

    public function __construct(
        public string $called,
        public string $abstract,
        public array $parameters) {
            $this->model = $parameters['model'];
    }

}
