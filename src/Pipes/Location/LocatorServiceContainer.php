<?php

namespace Symbiont\Services\Pipes\Location;

use Symbiont\Services\Pipes\Transportable;

class LocatorServiceContainer {

    public function handle(Transportable $transporter, \Closure $next) {
        // @todo: get $compact externally e.g. getCompactName
        if(app()->bound(($compact = $transporter->called.':'.$transporter->abstract))) {
            return app($compact, $transporter->parameters);
        }

        return $next($transporter);
    }

}