<?php

namespace Symbiont\Services\Pipes\Location;

use Symbiont\Services\Pipes\Transportable;

class LocatorAbstractIsClass {

    public function handle(Transportable $transporter, \Closure $next) {
        if(class_exists($transporter->abstract)) {
            return app($transporter->abstract, $transporter->parameters);
        }

        return $next($transporter);
    }

}