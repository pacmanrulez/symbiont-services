<?php

namespace Symbiont\Services\Pipes\Location;

use Symbiont\Services\Pipes\Transportable;

class LocatorModel {

    public function handle(Transportable $transporter, \Closure $next) {
        // @todo: interface_exists to distinguish string names from contracts
        if($abstract = $transporter->called::getService($transporter->abstract)) {
            return app($abstract, $transporter->parameters);
        }

        return $next($transporter);
    }

}