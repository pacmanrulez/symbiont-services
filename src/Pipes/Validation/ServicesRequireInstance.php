<?php

namespace Symbiont\Services\Pipes\Validation;

use Closure;
use Symbiont\Services\Casts\ContracterType;
use Symbiont\Services\Contracts\Serviceable;
use Symbiont\Services\Contracts\Validation\RequiresInstance;
use Symbiont\Services\Exceptions\ServiceRequiresInstance;

class ServicesRequireInstance {

    public function handle(Serviceable $service, Closure $next) {

        if($service instanceof RequiresInstance
        && $service->isType(ContracterType::Static)) {
            throw new ServiceRequiresInstance($service::class);
        }

        return $next($service);
    }

}