<?php

namespace Symbiont\Services\Pipes\Validation;

use Closure;
use Symbiont\Services\Contracts\Serviceable;
use Symbiont\Services\Exceptions\MissingSingleActionCall;
use Symbiont\Services\Types\SingleActionable;

class ValidateSingleActionables {

    const METHOD_HANDLE = 'handle';

    public function handle(Serviceable $service, Closure $next) {

        if($service instanceof SingleActionable) {
            if(is_callable($service)) {
                return app()->call($service);
            }
            elseif(method_exists($service, self::METHOD_HANDLE)) {
                return app()->call([$service, self::METHOD_HANDLE]);
            }

            throw new MissingSingleActionCall($service::class);
        }

        return $next($service);
    }

}