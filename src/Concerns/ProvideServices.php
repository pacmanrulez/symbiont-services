<?php

namespace Symbiont\Services\Concerns;

use Illuminate\Pipeline\Pipeline;
use Symbiont\Services\Contracts\HandlingServices;
use Symbiont\Services\Contracts\Serviceable;
use Symbiont\Services\Pipes\Validation\ServicesRequireInstance;
use Symbiont\Services\Pipes\Validation\ValidateSingleActionables;
use Symbiont\Services\ServiceHandler;

trait ProvideServices {

    const PROP_SERVICES = 'services';

    public static function setServices(array $services) {
        if(property_exists(self::class, self::PROP_SERVICES)) {
            self::${self::PROP_SERVICES} = array_merge(self::${self::PROP_SERVICES}, $services);
        }
    }

    /**
     * @return array
     */
    public static function getServices(): array {
        return property_exists(self::class, self::PROP_SERVICES) ?
            self::${self::PROP_SERVICES} : [];
    }

    /**
     * @param string $abstract
     * @return string|null
     */
    public static function getService(string $abstract): string|null {
        if(! property_exists(self::class, self::PROP_SERVICES)) {
            return null;
        }
        return self::${self::PROP_SERVICES}[$abstract] ?? null;
    }

    public function serve(string $abstract, array $input = []): mixed {
        return self::validateService(
            app(HandlingServices::class)::locateService(
                called: static::class,
                abstract: $abstract,
                instance: $this,
                input: $input )
        );
    }

    public static function serving(string $abstract, array $input = []): mixed {
        return self::validateService(
            app(HandlingServices::class)::locateService(
                called: static::class,
                abstract: $abstract,
                input: $input )
        );
    }

    /**
     * Validate service
     * @param Serviceable $service
     * @return Serviceable
     * @return mixed
     */
    protected static function validateService(Serviceable $service): mixed {
        return app(Pipeline::class)
            ->send($service)
            ->through([
                ServicesRequireInstance::class,
                ValidateSingleActionables::class ])
            ->thenReturn();
    }

}