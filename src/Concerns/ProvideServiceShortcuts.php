<?php

namespace Symbiont\Services\Concerns;

use Symbiont\Services\Types;

/**
 * @note: WIP
 */
trait ProvideServiceShortcuts {

    public function repository(array $input = []) {
        return $this->serve(Types\Repositorable::class, $input);
    }

    public static function repositorable(array $input = []) {
        return self::serving(Types\Repositorable::class, $input);
    }

    public function action(array $input = []) {
        return $this->serve(Types\Actionable::class, $input);
    }

    public static function actionable(array $input = []) {
        return self::serving(Types\Actionable::class, $input);
    }

    public function store(array $input = []) {
        return $this->serve(Types\Storeable::class, $input);
    }

    public static function storeable(array $input = []) {
        return self::serving(Types\Storeable::class, $input);
    }

}