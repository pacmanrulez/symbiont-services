<?php

namespace Symbiont\Services\Concerns;

/**
 * @note: WIP
 */
trait GoesThrough {

    protected string $through;

    public function through($through): self {
        $this->through = $through;
        return $this;
    }

}