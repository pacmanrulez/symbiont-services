<?php

namespace Symbiont\Services;

use Illuminate\Support\Facades\Facade;

class ServicesFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'symbiont-services';
    }
}
