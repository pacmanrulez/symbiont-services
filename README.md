# Symbiont/Services

This package is work in progress! Ideas go fly as they fly by! This package will go through ongoing development, if not by chance, of a proportional miniscule probability, I get a life.

## Requirements

- `php 8.2`
- `laravel/framework 11`

## Installation

```bash
composer require symbiont/services
```

## Documentation

This documentation only documents the technical usage of this package with little to no text. 
This package is created for personal use. The greater good has rarely outweighed my own self-interest. 
If anybody grabs the idea of what I'm doing here, feel free to use it. If not, don't bother.

- Documentation [https://pacmanrulez.gitlab.io/symbiont-services](https://pacmanrulez.gitlab.io/symbiont-services)
- Gitlab [https://gitlab.com/pacmanrulez/symbiont-services](https://gitlab.com/pacmanrulez/symbiont-services)

---

License bla author bla bla other cool stuff bla ..