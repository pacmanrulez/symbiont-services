# Creating service types

Using `SomeModel` model for all examples

```php
<?php

use App\Services\TestModel;

use Symbiont\Services\Types;

class SomeModel extends Model {
    
    protected static $services = [
        Types\Actionable::class => TestModel\Action::class,
        Types\SingleActionable::class => TestModel\SingleAction::class,
        Types\Repositorable::class => TestModel\Repository::class,
        Types\Storeable::class => TestModel\Store::class,
    ];
    
}

```

## Using standard types

| Type                      | For                                                                          |
|:--------------------------|------------------------------------------------------------------------------|
| `Actionable::class`       | Class for actions the model provides                                         |
| `SingleActionable::class` | Class for single actions the model provides executing `handle` or `__invoke` |
| `Repository::class`       | Class for repository matters                                                 |
| `Storable::class`         | For database transaction matters                                             |

## Directory structure

Depends on the discipline of putting stuff structured and organized in one place. By default, all services are being 
registered under `src/Services/<Model>/<Services>`. Generally speaking, all services can be saved anywhere.

### Actions

```php title="src/Services/SomeModel/Action.php"
<?php
namespace App\Services\TestModel;

use Symbiont\Services\Contracter;
use Symbiont\Services\Types\Actionable;

class Action extends Contracter 
    implements Actionable {

    public function someActionToDo() {
        // ..
    }

}

// non-static
(new TestModel)->serve(Actionable::class)
    ->someActionToDo();
// static
TestModel::serving(Actionable::class)
    ->someActionToDo();
```

### Single actions

```php title="src/Services/SomeModel/SingleAction.php"
<?php
namespace App\Services\TestModel;

use Symbiont\Services\Contracter;
use Symbiont\Services\Types\SingleActionable;

class SingleAction extends Contracter 
    implements SingleActionable {

    // using handle
    public function handle() {
        // ..
    }
    
    // or using invoke
    public function __invoke() {
        return $this->handle()
    }

}

// non-static
(new TestModel)->serve(SingleActionable::class);
// static
TestModel::serving(SingleActionable::class);
```

Single actions resolve services too.

```php


<?php

use Illuminate\Foundation\Application;
use Symbiont\Services\Contracts\HandlingServices;

class SingleAction ... {

    public function handle(HandlingServices $handler, Application $app) {
        $handler->toSomething($app);
    }

}
```

### Repositories

```php title="src/Services/SomeModel/Repository.php"
<?php
namespace App\Services\TestModel;

use Symbiont\Services\Contracter;
use Symbiont\Services\Types\Repositorable;

class Action extends Contracter 
    implements Repositorable {

    public function somethingRepositoriesDo() {
        // ..
    }
}

// non-static
(new TestModel)->serve(Repositorable::class)
    ->somethingRepositoriesDo();
// static
TestModel::serving(SingleActionable::class)
    ->somethingRepositoriesDo();
```

### Stores

```php title="src/Services/SomeModel/Stores.php"
<?php
namespace App\Services\TestModel;

use Symbiont\Services\Contracter;
use Symbiont\Services\Types\Storeable;

class Action extends Contracter 
    implements Storeable {

    public function somethingStoresDo() {
        // ..
    }
}

// non-static
(new TestModel)->serve(Storeable::class)
    ->somethingStoresDo();
// static
TestModel::serving(Storeable::class)
    ->somethingStoresDo();
```