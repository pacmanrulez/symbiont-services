# Usage

## Default use

Any service can be called non-statically via `serve` or statically using `serving`. 

```php
<?php
// non-static
(new SomeModel)->serve('some-service');
// static
SomeModel::serving('some-service');
```

!!!note
    When in static context using `SomeMode::serving('some-service')`, the service class does not provide an instance
    of the model (duh), e.g.

    ```php
    <?php
    class SomeService implements Contracter {
        
        public function someMethod() {
            $this->instance; // will be <null>
        }

    }
    ```



## Using shortcuts trait

| Non-Static       | Static              | Service                                     |
|------------------|---------------------|---------------------------------------------|
| `->action()`     | `::actionable()`    | retrieves the `Actionable::class` service   |
| `->repository()` | `::repositorable()` | retrieves the `Repositorable::class`service | 
| `->store()`      | `::storeable()`     | retrieve the `Storeable::class` service     |

```php
<?php

use Symbiont\Services\Concerns\ProvideServiceShortcuts;
use Symbiont\Services\Contracts\ProvidesServices;
use Symbiont\Services\Concerns\ProvideServices;

class SomeModel extends Model 
    implements ProvidesServices {

    use ProvideServices,
        ProvideServiceShortcuts;
}

// non-static
(new SomeModel)->action()
// static
SomeModel::actionable();
```

!!!note
    Statically calling `::actionable()` will not deliver any model instance.     

## Passing additional data

```php
<?php

(new SomeModel)->serve(Repositorable::class, [
    'key' => 'some-value'
])

SomeModel::serving(Repositorable::class, [
    'key' => 'some-value'
]);

SomeModel::repository([
    'key' => 'some-value'
])

class Repository extends Contracter
    implements Repositorable {
    
    public function somethingRepositoriesDo() {
        return $this->input['key'] // returns 'some-value'
    }    
    
}
```