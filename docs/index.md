# Services

!!! warning
    This package is `work in progress`! Ideas go fly as they fly by! This package will go through ongoing development, if not by chance, of a proportional miniscule probability, I get a life.

!!! Note
    This documentation only documents the technical usage of this package with little to no text. This package is created for personal use. The greater good has rarely outweighed my own self-interest. If anybody grabs the idea of what I'm doing here, feel free to use it. If not, don't bother.

## Installation

Requires at least PHP `8.2` and Laravel `11`

```bash
composer require symbiont/services
```

---

## Config

@todo: publishing

## Testing

```php
./vendor/pestphp/pest/bin/pest
```

