# Registering services

## Locally at any model

```php
<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Services\SomeModel\SomeAction;

use Symbiont\Services\Contracts\ProvidesServices;
use Symbiont\Services\Concerns\ProvideServices;

use Symbiont\Services\Types\Actionable;

class SomeModel extends Model 
    implements ProvidesServices {

    use ProvideServices;
    
    protected static $services = [
        // by contract
        Actionable::class => SomeAction::class
        // by description
        'some-action' => SomeAction::class
    ]
}
```

!!!note
    By default, the `protected static $services` is not defined. It should be defined if needed.

Or defined in one place using a service provider.

```php
<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Models\SomeModel;
use App\Services\SomeModel\SomeAction;

use Symbiont\Services\Types\Actionable;

class ServiceableProvider extends ServiceProvider {

    public function register() {
        // request ServiceHandler
        SomeModel::setServices([
            // by contract
            Actionable::class => SomeAction::class
            // by description
            'some-action' => SomeAction::class
        ]);
        // or if $services is a public static
        SomeModel::$services = [
            // .. services
        ];

    }

}
```

## Via ServiceHandler

```php
<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\SomeModel;

class ServiceableProvider extends ServiceProvider {

    public function register() {
        // request ServiceHandler
        app(HandlingServices::class)::services(SomeModel::class, [
            // by contract
            Actionable::class => SomeAction::class
            // by description
            'some-action' => SomeAction::class
        ]);
    }

}
```

Register for all models omitting the model class, equivalent of using a wildcard `*`.

```php
<?php
        app(HandlingServices::class)::services([
            // by contract
            Actionable::class => SomeAction::class
            // by description
            'some-action' => SomeAction::class
        ]);
```

## Via service container

```php
<?php
class ServiceableProvider extends ServiceProvider {

    public function register() {
        // by contract
        $this->app->bind(
           SomeModel::class.':'.Actionable::class,
           SomeAction::class
        );
        // by description
        $this->app->bind(
           SomeModel::class.':some-action',
           SomeAction::class
        );
    }

}
```