<?php

use Illuminate\Foundation\Application;

use Symbiont\Services\Tests\Models\TestModel;
use Symbiont\Services\Tests\TestModel\{
    Action, Repository, Store, SingleActionHandle, SingleActionInvoke
};
use Symbiont\Services\Types\{
    Actionable
};

use Symbiont\Services\Tests\Dependencies\{
    SomeDependency, IsDependable
};

use Symbiont\Services\Exceptions\{
    MissingSingleActionCall,
    UnknownServiceableMethod,
    ServiceRequiresInstance,
    ServiceNotFound
};

use Symbiont\Services\ServiceHandler;

/**
 * Simple setup with a TestModel containing 4 services
 *  - Actionable::class         => Action::class,
 *     - used for service shortcut `action()`
 *  - 'single-action'           => SingleAction::class,
 *  - 'single-action-handle'    => SingleActionHandle::class,
 *  - 'single-action-invoke'    => SingleActionInvoke::class
 *
 * ServiceHandler registering a repository service
 *  - 'repository' => Repository::class
 *    - used for service shortcut `repository()` to fail
 *
 * Service Container registering a store service
 *  - app()->bind('store', Store::class);
 *
 * - The SingleAction::class does not implement handle or invoke
 * - The Store service requires a model instance (cannot be statically called)
 * - The Action Service includes all method call tests (DI, arguments, input)
 */

beforeEach(function() {
    $this->instance = new TestModel();

    app()->bind(
        IsDependable::class,
        function() {
            return new SomeDependency();
        }
    );

    ServiceHandler::services(TestModel::class, [
        'repository' => Repository::class,
    ]);

    app()->bind(TestModel::class.':store', Store::class);
});

test('Get Action::class service by Actionable::class contract', function () {
    $action = $this->instance->serve(Actionable::class);

    expect($action)
        ->toBeInstanceOf(Action::class);

    expect($action->test())
        ->toBeString('test-call');
});

test('Service Action::class DI', function() {
    $action = $this->instance->serve(Actionable::class);

    expect($action)
        ->toBeObject()
        ->toBeInstanceOf(Action::class);

    expect(app()->bound(Application::class))
        ->toBeTrue();

    // normal call with arguments
    expect($action->testDependency(app()))
        ->toBeInstanceOf(Application::class);

    // resoveld returns Application $app
    expect($action->_testDependency())
        ->toBeInstanceOf(Application::class);

    expect(app()->bound(IsDependable::class))
        ->toBeTrue();

    // normal call with arguments
    expect($action->testBindDependency(new SomeDependency))
        ->toBeInstanceOf(IsDependable::class);

    // returns SomeDependency $dep
    expect($action->_testBindDependency())
        ->toBeInstanceOf(IsDependable::class);
});

test('Service Action::class DI with arguments', function() {
    expect($this->instance->serve(Actionable::class)->_testArgumentWithDependency(custom: 'with-arguments'))
        ->toBeString('with-arguments');
});

test('SingleAction service by string name missing call handle/invoke', function() {
    expect(fn() => $this->instance->serve('single-action'))
        ->toThrow(MissingSingleActionCall::class);
});

test('SingleAction handle/invoke', function() {
    $single = $this->instance->serve('single-action-handle');
    expect($single)
        ->toBeString('woohoo!');

    $single = $this->instance->serve('single-action-invoke');
    expect($single)
        ->toBeString('boeja!');
});

test('Missing service method', function() {
    expect(fn() => $this->instance->serve(Actionable::class)->doesNotExist())
        ->toThrow(UnknownServiceableMethod::class);
});

test('Through ServiceHandler::class returning Repository::class service', function() {
    // should be called from ServiceHandler
    $repository = $this->instance->serve('repository');
    expect($repository)
        ->toBeObject()
        ->toBeInstanceOf(Repository::class);
});

test('TestModel static call', function() {
    $repository = TestModel::serving('repository');
    expect($repository)
        ->toBeObject()
        ->toBeInstanceOf(Repository::class);
});

test('Service not found', function() {
    expect(fn() => $this->instance->serve('does-not-exist'))
        ->toThrow(ServiceNotFound::class);
});

test('Service container has a bound store service', function() {
    expect(app()->bound(TestModel::class.':store'))
        ->toBeTrue();
});

test('Through Service Container returning Store::class service', function() {
    $store = $this->instance->serve('store');
    expect($store)
        ->toBeObject()
        ->toBeInstanceOf(Store::class);
});

test('TestModel static call where service requires model instance', function() {
    expect(fn() => $this->instance::serving('store'))
        ->toThrow(ServiceRequiresInstance::class);
});

test('TestModel service shortcut `action()`', function() {
    $action = $this->instance->action();

    expect($action)
        ->toBeObject()
        ->toBeInstanceOf(Action::class)
        ->toBeInstanceOf(Actionable::class);
});

test('Service Action::class method call with ServerHandler DI returning repository string class', function() {
    expect($this->instance->action()->_testServiceHandler())
        ->toBeString(Repository::class);
});

test('Service Action::class with additional input', function() {
    expect($this->instance->serve(Actionable::class, ['some-input' => 'some-value'])
        ->getInputByKey('some-input'))
        ->toBeString('some-value');

    expect($this->instance->action([
        'some-input' => 'some-value'
    ])->getInputByKey('some-input'))->toBeString('some-value');
});