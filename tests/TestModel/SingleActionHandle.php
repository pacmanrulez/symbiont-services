<?php

namespace Symbiont\Services\Tests\TestModel;

use Symbiont\Services\Contracter;
use Symbiont\Services\Types\SingleActionable;

class SingleActionHandle extends Contracter
    implements SingleActionable {

    public function handle(): string {
        return 'woohoo!';
    }

}
