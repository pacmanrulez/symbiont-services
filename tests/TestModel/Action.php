<?php

namespace Symbiont\Services\Tests\TestModel;

use Illuminate\Foundation\Application;
use Symbiont\Services\Contracter;
use Symbiont\Services\Contracts\HandlingServices;
use Symbiont\Services\Tests\Dependencies\IsDependable;
use Symbiont\Services\Tests\Dependencies\SomeDependency;
use Symbiont\Services\Types\Actionable;

class Action extends Contracter
    implements Actionable {

    public function test(): string {
        return 'test-call';
    }

    public function testDependency(Application $app): Application {
        return $app;
    }

    public function testBindDependency(IsDependable $dep) {
        return $dep;
    }

    public function testArgumentWithDependency(string $custom, IsDependable $dep) {
        return $custom;
    }

    public function testServiceHandler(HandlingServices $handler): string {
        return $handler::service('repository', $this->model);
    }

    public function getInputByKey(string $key): string|null {
        return $this->input[$key] ?? null;
    }

}
