<?php

namespace Symbiont\Services\Tests\TestModel;

use Symbiont\Services\Contracter;
use Symbiont\Services\Contracts\Validation\RequiresInstance;
use Symbiont\Services\Types\Storeable;

class Store extends Contracter
    implements Storeable, RequiresInstance { }