<?php

namespace Symbiont\Services\Tests\TestModel;

use Symbiont\Services\Contracter;
use Symbiont\Services\Types\SingleActionable;

class SingleActionInvoke extends Contracter
    implements SingleActionable {

    public function __invoke(): string {
        return 'boeja!';
    }

}
