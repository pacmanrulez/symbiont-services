<?php

namespace Symbiont\Services\Tests\TestModel;

use Symbiont\Services\Contracter;
use Symbiont\Services\Types\SingleActionable;

class SingleAction extends Contracter
    implements SingleActionable {}
