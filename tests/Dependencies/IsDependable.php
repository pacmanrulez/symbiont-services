<?php

namespace Symbiont\Services\Tests\Dependencies;

/**
 * Test class for DI using app()
 */
interface IsDependable {

    public function mockMe(?string $value): string;

}