<?php

namespace Symbiont\Services\Tests\Dependencies;

/**
 * Test class for DI using app()
 */
class SomeDependency implements IsDependable {

    public function mockMe(?string $value): string {
        return 'mocked!' . $value;
    }

}