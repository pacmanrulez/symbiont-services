<?php

namespace Symbiont\Services\Tests\Models;

use Illuminate\Database\Eloquent\Model;
use Symbiont\Services\Concerns\ProvideServices;
use Symbiont\Services\Concerns\ProvideServiceShortcuts;
use Symbiont\Services\Contracts\ProvidesServices;
use Symbiont\Services\Tests\TestModel\{
    Action, SingleAction, SingleActionHandle, SingleActionInvoke
};
use Symbiont\Services\Types\Actionable;

class TestModel extends Model
    implements ProvidesServices {

    use ProvideServices,
        ProvideServiceShortcuts;

    protected static $services = [
        Actionable::class => Action::class,
        'single-action' => SingleAction::class,
        'single-action-handle' => SingleActionHandle::class,
        'single-action-invoke' => SingleActionInvoke::class
    ];

    public function testing() {
        return 'testing';
    }

}