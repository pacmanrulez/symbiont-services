<?php

use Symbiont\Services\Tests\Models\TestModel;
use Symbiont\Services\Contracts\ProvidesServices;

beforeEach(function() {
    $this->model = new TestModel();
});

test('test if TestModel exists', function () {
    expect($this->model->testing())->toBeString('testing');
});

test('test if TestModel contains services', function() {
    expect($this->model instanceof ProvidesServices)->toBeTrue();
});

